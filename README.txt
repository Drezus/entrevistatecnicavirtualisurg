VIRTUALISURG VOLLEY
by André Segato

[INSTALLATION]

Unzip the corresponding zip archive(s) located in the repository's root directory for the platform(s) you want to test on.

- Desktop standalones doesn't require installation, just run the corresponding .exe or .app binary.
- For Android, it is necessary to install the provided .apk file. A message warning about "App not compatible with latest version of Android" can be safely ignored.

*iOS debug app distribution requires a paid Apple Developer account, so I couldn't provide a standalone .ipa file. If you wish to test on iOS, please use the provided Unity source project to generate an Xcode project and deploy manually to your own iOS device using your own Apple account app signing. Sorry for the inconvenience.

Upon first run, the app may ask for firewall or network privileges. Allow those as they're required for online play.


[CONTROLS]

-DESKTOP STANDALONE-
--PLAYER 1 (BLUE):
Move/Control the spike crosshair: WASD
Jump/Spike: Spacebar
--PLAYER 2 (RED):
Move/Control the spike crosshair: Arrow Keys
Jump/Spike: Enter/Return

-MOBILE PLATFORMS-
Move/Control the spike crosshair: Left side on-screen joystick
Jump/Spike: Right side on-screen button


[GAMEPLAY ACTIONS]

LOBS: Grounded hits that send the ball high over your own court. Useful for repositioning or preparing for a spike.
To lob, just stand below the ball and let it hit you. As long as you're on the ground, the ball will be lobbed automatically.

AIMING SPIKES: Jump while the ball is heading your side of the court and a colored spike crosshair will appear at the opponent's side of the court.
While airborne, pressing the Move buttons will instead move this crosshair, allowing you to aim exactly where you want to spike the ball. The crosshair will disappear if you land without spiking the ball.

SPIKING:
Press the "Jump/Spike" button while airborne and within spike range of the ball. Your spike crosshair will lock in place and the ball will be launched to the location you aimed your spike crosshair at.


[GAMEPLAY TIPS AND NOTES]

*Jumping while running will mantain your momentum until you land back on the ground. Unless you're trying to reach for a very far spike, it's usually safer to stop moving before jumping.

*Players WILL NOT lob the ball while jumping. If you try to, the ball will pass right through you (this is by design!). When jumping, you either have to commit for a spike, or give up and land to lob the incoming ball.

*Lobbing the ball for a third time will instead lob the ball into the opponent's court, giving them a very slow and easy to catch ball. 
Be sure to try and go for a spike before your third ball hit so you'll have a much greater chance to score.

*If you attempted to spike the ball and nothing happened, you might've been too far away to hit it.

*While aiming a spike, be aware of the trajectory the ball will take. Spiking near the net will almost always result in a net penalty, unless you're close to the net yourself.


[GAME MODES]

*All game modes are 7-points-to-win matches and restart automatically after a few seconds once the game is over.

-SINGLEPLAYER-
Assuming P1 controls (the blue player), play a match against a CPU-controlled P2 red player.
*The CPU chases any lob and spike markers on their side of the court.
*Upon optimal conditions (being very close to the ball's predicted landing spot while the ball is high and close enough), the CPU will go for a spike.
*The CPU aims for random spots on your court when going for a spike. It might even try to spike too close to the net and get a net penalty. But that's alright, we're all humans (not literally) and prone to errors. :)

-LOCAL MULTIPLAYER-
P1 plays a match against a human-controlled P2. P1 controls the blue player, P2 controls the red player.
*This mode is unavailable in mobile platforms and won't appear on the main menu.

-ONLINE MULTIPLAYER-
Host an online match and share a Room Code to invite another player, or join another player's room using a Room Code.
The host will assume the P1 blue player, and the joining player will assume the P2 red player.
*When hosting, you'll only be able to press the "Start Game" button to being the match once another player enters the room.
*When playing on desktop standalone platforms, both the host and the joiner use Player 1 keys to control their respective players.
*If one or both players disappears, it means someone left the room or disconnected. Just use the "Back to Menu" button to restart and try again.
*PLEASE NOTE* that this mode is hosted on a free tier on Unity Cloud's Relay service responsible for generating room codes and connecting player to the best possible servers. As a free tier, the service is prone to lag and subject to instability. Thank you for understanding.

-DEMO MODE-
A CPU-controlled P1 blue player plays a match against another CPU-controlled P2 red player.
*On mobile platforms, the on-screen joystick and button inputs will not appear on this mode since the user doesn't control neither player.


Thanks for playing!


