using System;
using System.Collections.Generic;
using UnityEngine;

[Serializable]
public struct PlayerSettings
{
    public uint id;
    public Color color;
    public InputSettings inputSet;
}

[CreateAssetMenu(fileName = "Assets/ScriptableObjects/New GameParameters", menuName = "VirtualiSurg Volley/Game Parameters")]
public class GameParameters : ScriptableObject
{
    public List<PlayerSettings> playerSettings;

    [Header("Match rules")]
    public int pointsToWin = 7;
    public int lobCountLimit = 2; //How many lobs each player can make on the ball before having to pass it to the other court.

    [Header("Player physics")]
    public float playerGravity = Physics.gravity.y;
    public float playerSpeed;
    public float playerJumpForce;
    public Rect leftCourtLimits, rightCourtLimits;
    public float playerHeightFromFloor;
    public float collisionDistance; //How close the ball has to be from a player to be considered hit by them.
    public float spikeRange; //How far players can be from the ball and still be able to spike them.

    [Header("Ball physics")]
    public float ballGravity = Physics.gravity.y;
    public float lobHeight; //How high the ball goes when bounced off from a grounded player.
    public float ballHeightFromFloor;
    public float maxLobDistance = 1f; //How far a lob can go from the player that lobbed it.

    [Header("Spike Marker variables")]
    public float markerSpeed;
}