using UnityEngine;

[CreateAssetMenu(fileName = "Assets/ScriptableObjects/New PlayerInputs", menuName = "VirtualiSurg Volley/Player Input set")]
public class InputSettings : ScriptableObject
{
    [Header("Note: Use names that are already defined in Unity's own Input Manager system.")]
    public string horizontal;
    public string vertical;
    public string jump;
}