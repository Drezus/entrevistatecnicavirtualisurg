using System.Threading.Tasks;
using TMPro;
using Unity.Netcode;
using Unity.Netcode.Transports.UTP;
using Unity.Networking.Transport.Relay;
using Unity.Services.Authentication;
using Unity.Services.Core;
using Unity.Services.Relay;
using Unity.Services.Relay.Models;
using UnityEngine;
using UnityEngine.UI;

public class NetworkGameManager : NetworkBehaviour
{
    [SerializeField] private GameManager gameManager;

    [Header("UI References")]
    [SerializeField] private Canvas onlinePlayCanvas;
    [SerializeField] private GameObject hostJoinMenuParent;
    [SerializeField] private Button hostBtn;
    [SerializeField] private Button joinBtn;
    [SerializeField] private TMP_InputField joinCodeField;

    [SerializeField] private Button startGameBtn;
    [SerializeField] private TMP_Text startGameBtnText;

    [SerializeField] private TMP_Text statusTxt;

    void Awake()
    {
        statusTxt.text = string.Empty;

        hostBtn.onClick.AddListener(StartHosting);
        joinBtn.onClick.AddListener(StartJoining);
        startGameBtn.onClick.AddListener(StartGameRpc);
    }

    public override void OnDestroy()
    {
        hostBtn.onClick.RemoveAllListeners();
        joinBtn.onClick.RemoveAllListeners();
        startGameBtn.onClick.RemoveAllListeners();
        base.OnDestroy();
    }

    private async void StartHosting()
    {
        statusTxt.text = "";
        string roomCode = await HostWithRelay();

        statusTxt.text = !string.IsNullOrEmpty(roomCode) ? $"Room Code: <color=orange>{roomCode}</color>" : "<color=red>Error: Couldn't create room";

        if (string.IsNullOrEmpty(roomCode)) return;

        hostJoinMenuParent.SetActive(false);
        startGameBtn.gameObject.SetActive(true);
    }

    private async void StartJoining()
    {
        statusTxt.text = "Connecting...";

        if (string.IsNullOrEmpty(joinCodeField.text))
        {
            statusTxt.text = "<color=red>Enter a valid Room Code!";
            return;
        }

        bool success = await JoinWithRelay(joinCodeField.text.ToUpperInvariant());

        statusTxt.text = success? $"Room joined! Waiting for host to start game..." : "<color=red>Error: Couldn't join room";

        if (!success) return;

        hostJoinMenuParent.SetActive(false);
    }

    [Rpc(SendTo.Everyone, RequireOwnership = false)]
    private void StartGameRpc()
    {
        onlinePlayCanvas.gameObject.SetActive(false);
        if(IsServer) gameManager.NewMatch();
    }

    public override void OnNetworkSpawn()
    {
        base.OnNetworkSpawn();
        SpawnPlayerRpc(NetworkManager.Singleton.LocalClientId);
    }

    private async Task<string> HostWithRelay(int maxConnections=5)
    {
        await UnityServices.InitializeAsync();
        if (!AuthenticationService.Instance.IsSignedIn)
        {
            await AuthenticationService.Instance.SignInAnonymouslyAsync();
        }
        Allocation allocation = await RelayService.Instance.CreateAllocationAsync(maxConnections);
        NetworkManager.Singleton.GetComponent<UnityTransport>().SetRelayServerData(new RelayServerData(allocation, "dtls"));
        string joinCode = await RelayService.Instance.GetJoinCodeAsync(allocation.AllocationId);
        Debug.Log($"Join Code:{joinCode}");
        return NetworkManager.Singleton.StartHost() ? joinCode : null;
    }

    private async Task<bool> JoinWithRelay(string roomCode)
    {
        await UnityServices.InitializeAsync();
        if (!AuthenticationService.Instance.IsSignedIn)
        {
            await AuthenticationService.Instance.SignInAnonymouslyAsync();
        }

        JoinAllocation joinAllocation = await RelayService.Instance.JoinAllocationAsync(roomCode);
        NetworkManager.Singleton.GetComponent<UnityTransport>().SetRelayServerData(new RelayServerData(joinAllocation, "dtls"));
        return NetworkManager.Singleton.StartClient();
    }

    [Rpc(SendTo.Server, RequireOwnership = false)]
    private void SpawnPlayerRpc(ulong clientID)
    {
        GameObject player = gameManager.SpawnNewPlayer(false);
        NetworkObject playerNetObj = player.GetComponent<NetworkObject>();
        playerNetObj.SpawnWithOwnership(clientID,true);

        GameObject spikeMarker = gameManager.SpawnNewSpikeMarker();
        NetworkObject markerNetObj = spikeMarker.GetComponent<NetworkObject>();
        markerNetObj.SpawnWithOwnership(clientID,true);

        gameManager.SetPlayersControls(true);

        SetupExistingPlayersRpc();

        if (gameManager.SpawnedPlayersAmount < 2) return;

        startGameBtn.interactable = true;
        startGameBtnText.text = "Start Game!";
    }

    [Rpc(SendTo.Everyone, RequireOwnership = false)]
    private void SetupExistingPlayersRpc()
    {
        foreach (PlayerManager pMng in FindObjectsOfType<PlayerManager>())
        {
            pMng.Setup((uint)pMng.GetComponent<NetworkObject>().OwnerClientId, gameManager);
        }

        foreach (SpikeCrosshairMovement scm in FindObjectsOfType<SpikeCrosshairMovement>())
        {
            scm.Setup((uint)scm.GetComponent<NetworkObject>().OwnerClientId);
        }
    }
}