using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using TMPro;
using Unity.Netcode;
using Unity.VisualScripting;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using Random = UnityEngine.Random;

public enum GameModes
{
    Singleplayer,
    LocalMultiplayer,
    OnlineMultiplayer,
    Demo
}

public class GameManager : NetworkBehaviour
{
    public GameParameters gameParams;

    public GameModes CurrentGameMode
    {
        get;
        private set;
    }

    [Header("Static Scene References")]
    [SerializeField] private TMP_Text scoreText;
    [SerializeField] private BallMovement ball;
    [SerializeField] public LobTarget lobMarker;
    [SerializeField] public Canvas onlinePlayCanvas;

    [Header("Mobile Input References")]
    [SerializeField] public Canvas mobileInputCanvas;
    [SerializeField] public MobileJoystick joystick;
    [SerializeField] public Button jumpBtn;

    [Header("Dynamic Prefabs")]
    [SerializeField] private GameObject humanPlayerPrefab;
    [SerializeField] private GameObject cpuPlayerPrefab;
    [SerializeField] public GameObject spikeMarkerPrefab;

    private List<BasePlayerMovement> playerMovs;
    private List<PlayerInputs> playerInputs;
    private List<SpikeCrosshairMovement> markers;

    public uint SpawnedPlayersAmount => (uint)playerMovs.Count;

    private int leftCourtScore, rightCourtScore = 0;

    private Color p1Color => gameParams.playerSettings.FirstOrDefault(listEntry => listEntry.id == 0).color;
    private Color p2Color => gameParams.playerSettings.FirstOrDefault(listEntry => listEntry.id == 1).color;

    private bool gameIsOver => rightCourtScore >= gameParams.pointsToWin || leftCourtScore >= gameParams.pointsToWin;
    private bool isOnlineClient => CurrentGameMode == GameModes.OnlineMultiplayer && !IsServer;

    public bool canMovePlayers
    {
        get;
        private set;
    }

    public sealed class GameManagerEvent : UnityEvent { };
    public readonly GameManagerEvent OnCourtReset = new GameManagerEvent();

    private void Awake()
    {
        playerMovs = new List<BasePlayerMovement>();
        playerInputs = new List<PlayerInputs>();
        markers = new List<SpikeCrosshairMovement>();

        ball.OnLand.AddListener(() => StartCoroutine(Score()));
        ball.OnNetHit.AddListener(() =>
        {
            ball.freezePhysics = true;
            StartCoroutine(Score(true));
        });
        ball.freezePhysics = true;
    }

    public override void OnDestroy()
    {
        if(ball) ball.OnLand.RemoveAllListeners();
        if(ball) ball.OnNetHit.RemoveAllListeners();
        base.OnDestroy();
    }

    public void StartGameMode(int mode)
    {
        CurrentGameMode = (GameModes)mode;

        mobileInputCanvas.gameObject.SetActive(CurrentGameMode != GameModes.Demo && Application.platform is RuntimePlatform.Android or RuntimePlatform.IPhonePlayer);

        if (CurrentGameMode == GameModes.OnlineMultiplayer)
        {
            onlinePlayCanvas.gameObject.SetActive(true);
            return;
        }

        for (int i = 0; i < 2; i++)
        {
            bool isCPU = false;
            if (CurrentGameMode == GameModes.Demo) isCPU = true;
            else isCPU = CurrentGameMode == GameModes.Singleplayer && i > 0;

            GameObject p = SpawnNewPlayer(isCPU);
            p.GetComponent<PlayerManager>().Setup((uint)i, this);
            GameObject spk = SpawnNewSpikeMarker();
            spk.GetComponent<SpikeCrosshairMovement>().Setup((uint)i, isCPU);
        }

        NewMatch();
    }

    public void ResetScene()
    {
        Destroy(FindObjectOfType<NetworkManager>().gameObject);

        Scene scene = SceneManager.GetActiveScene();
        SceneManager.LoadScene(scene.name);
    }

    public GameObject SpawnNewPlayer(bool isCPU)
    {
        if (playerMovs.Count >= 2) return null;

        GameObject newPlayer = Instantiate(isCPU ? cpuPlayerPrefab : humanPlayerPrefab);
        playerMovs.Add(newPlayer.GetComponent<BasePlayerMovement>());

        if (isCPU) return newPlayer;

        playerInputs.Add(newPlayer.GetComponent<PlayerInputs>());
        return newPlayer;
    }

    public GameObject SpawnNewSpikeMarker()
    {
        if (markers.Count >= 2) return null;

        SpikeCrosshairMovement newMarker = Instantiate(spikeMarkerPrefab, Vector3.zero, Quaternion.identity).GetComponent<SpikeCrosshairMovement>();
        markers.Add(newMarker);

        return newMarker.gameObject;
    }

    public void NewMatch()
    {
        leftCourtScore = rightCourtScore = 0;
        UpdateScoreboard();
        StartCoroutine(NewRound(gameParams.leftCourtLimits));
    }

    private IEnumerator NewRound(Rect startingCourtSide)
    {
        SetPlayersControls(false);
        CourtReset();

        yield return new WaitForSeconds(1f);

        SetPlayersControls(true);

        if (isOnlineClient) yield break;

        Vector3 randomPos = new Vector3(
            Random.Range(startingCourtSide.x - (startingCourtSide.width / 2), startingCourtSide.x + (startingCourtSide.width / 2)),
            gameParams.ballHeightFromFloor,
            Random.Range(startingCourtSide.y - (startingCourtSide.height / 2), startingCourtSide.y + (startingCourtSide.height / 2)));

        lobMarker.SetMarkerAtPos(randomPos);
        ball.LobAtTarget(lobMarker, gameParams.lobHeight);
    }

    private IEnumerator Score(bool netPenalty = false)
    {
        SetPlayersControls(false);

        if (netPenalty)
        {
            if (ball.IsTargettingLeftCourt) leftCourtScore++;
            else rightCourtScore++;
        }
        else
        {
            if (ball.transform.position.x <= 0) rightCourtScore++;
            else leftCourtScore++;
        }

        UpdateScoreboard();

        if (gameIsOver)
        {
            yield return new WaitForSeconds(6f);
            NewMatch();
        }
        else
        {
            yield return new WaitForSeconds(2f);

            Rect nextCourt = !netPenalty ?
                (ball.transform.position.x <= 0 ? gameParams.leftCourtLimits : gameParams.rightCourtLimits) :
                (ball.IsTargettingLeftCourt ? gameParams.rightCourtLimits : gameParams.leftCourtLimits);

            StartCoroutine(NewRound(nextCourt));
        }
    }

    private void CourtReset()
    {
        ball.transform.position = new Vector3(0f, 0.8f, 2f);
        ball.freezePhysics = true;

        OnCourtReset?.Invoke();

        foreach (ITargetMarker m in markers)
        {
            m.SetMarkerVisibility(false);
        }
    }

    private void UpdateScoreboard()
    {
        if (isOnlineClient) return;

        if (!gameIsOver)
        {
            scoreText.text =
                $"<color=#{p1Color.ToHexString()}>{leftCourtScore}</color>-<color=#{p2Color.ToHexString()}>{rightCourtScore}</color>";
        }
        else
        {
            scoreText.text = leftCourtScore > rightCourtScore ?
                $"<color=#{p1Color.ToHexString()}>P1</color> <size=35>VICTORY!" : $"<color=#{p2Color.ToHexString()}>P2</color> <size=35>VICTORY!";
        }

        if (IsSpawned) ReplicateScoreboardOnClientRpc(scoreText.text);
    }

    [Rpc(SendTo.NotServer, RequireOwnership = false)]
    private void ReplicateScoreboardOnClientRpc(string txt)
    {
        scoreText.text = txt;
    }

    public void SetPlayersControls(bool canMove)
    {
        if (IsSpawned)
        {
            SetPlayersControlsRpc(canMove);
        }
        else
        {
            canMovePlayers = canMove;
        }
    }

    [Rpc(SendTo.Everyone, RequireOwnership = false)]
    private void SetPlayersControlsRpc(bool canMove)
    {
        canMovePlayers = canMove;
    }
}