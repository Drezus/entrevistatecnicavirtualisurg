using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlatformDependantVisibility : MonoBehaviour
{
    [SerializeField] private List<RuntimePlatform> platformsToHide;

    private void Awake()
    {
        if(platformsToHide.Contains(Application.platform))
            gameObject.SetActive(false);
    }
}