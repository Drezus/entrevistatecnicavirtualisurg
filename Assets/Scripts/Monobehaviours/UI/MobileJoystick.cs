using UnityEngine;
using UnityEngine.EventSystems;

public class MobileJoystick : MonoBehaviour, IDragHandler, IPointerDownHandler, IPointerUpHandler
{
    [SerializeField] private RectTransform bg;
    [SerializeField] private RectTransform handle;

    [SerializeField, Range(0f, 1f)]
    private float handleLimits = 0.5f;

    [SerializeField] private Camera cam;

    private Vector2 input = Vector2.zero;

    public float Horizontal => input.x;
    public float Vertical => input.y;

    public void OnDrag(PointerEventData eventData)
    {
        Vector2 dir = eventData.position - RectTransformUtility.WorldToScreenPoint(new Camera(), bg.position);
        input = dir.magnitude > bg.sizeDelta.x / 2f ? dir.normalized : dir/ (bg.sizeDelta.x/2f);

        handle.anchoredPosition = (input * bg.sizeDelta.x / 2f) * handleLimits;
    }

    public void OnPointerDown(PointerEventData eventData)
    {
        OnDrag(eventData);
    }

    public void OnPointerUp(PointerEventData eventData)
    {
        input = Vector2.zero;
        handle.anchoredPosition = bg.anchoredPosition;
    }
}