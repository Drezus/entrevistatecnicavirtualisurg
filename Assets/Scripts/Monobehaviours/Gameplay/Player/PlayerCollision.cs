using System;
using UnityEngine;
using UnityEngine.Events;
using Random = UnityEngine.Random;

public class PlayerCollision : MonoBehaviour
{
    [SerializeField] private Transform thisTransform;
    [SerializeField] private GameParameters gameParams;

    private BasePlayerMovement _basePlayerMov;
    private LobTarget lobTarget;

    protected sealed class PlayerCollisionEvent : UnityEvent { };
    protected readonly PlayerCollisionEvent OnBallLobbed = new PlayerCollisionEvent();

    private int lobCount = 0;

    private bool collisionTriggered;
    private bool isCollidingWithBall => Vector3.Distance(thisTransform.position, ball.transform.position) <= gameParams.collisionDistance;

    private BallMovement ball;

    private GameManager gameManager;

    private void Awake()
    {
        ball = FindObjectOfType<BallMovement>();

        OnBallLobbed.AddListener(() =>
        {
            collisionTriggered = true;
            LobBall();
        });

        gameManager = FindObjectOfType<GameManager>();
    }

    public void SetupInstance(BasePlayerMovement mov, LobTarget lobTgt)
    {
        _basePlayerMov = mov;
        lobTarget = lobTgt;
    }

    private void Start()
    {
        lobCount = 0;
        collisionTriggered = false;
    }

    private void OnDestroy()
    {
        OnBallLobbed.RemoveAllListeners();
    }

    private void Update()
    {
        if (isCollidingWithBall && !collisionTriggered && _basePlayerMov.IsGrounded && gameManager.canMovePlayers) OnBallLobbed?.Invoke();

        if (!isCollidingWithBall && collisionTriggered) collisionTriggered = false;
    }

    private void LobBall()
    {
        lobCount++;

        //If the player's lob count exceeds the game parameter's, lob to the other court instead.
        Rect thisPlayerCourt = _basePlayerMov.PlayerID == 0 ? gameParams.leftCourtLimits : gameParams.rightCourtLimits;
        Rect otherCourt = _basePlayerMov.PlayerID == 0  ? gameParams.rightCourtLimits : gameParams.leftCourtLimits;

        Rect targetCourt = lobCount <= gameParams.lobCountLimit ? thisPlayerCourt : otherCourt;
        float clampedRandomX = Mathf.Clamp(targetCourt.x + Random.Range(-gameParams.maxLobDistance, gameParams.maxLobDistance), targetCourt.x - (targetCourt.width/2), targetCourt.x + (targetCourt.width/2));
        float clampedRandomZ = Mathf.Clamp(targetCourt.y + Random.Range(-gameParams.maxLobDistance, gameParams.maxLobDistance), targetCourt.y - (targetCourt.height/2), targetCourt.y + (targetCourt.height/2));

        Vector3 randomPos = new Vector3(clampedRandomX, gameParams.ballHeightFromFloor, clampedRandomZ);

        lobTarget.transform.position = randomPos;
        ball.LobAtTarget(lobTarget, gameParams.lobHeight);

        if(targetCourt == otherCourt) ResetLobCount();
    }

    public void ResetLobCount()
    {
        lobCount = 0;
    }
}