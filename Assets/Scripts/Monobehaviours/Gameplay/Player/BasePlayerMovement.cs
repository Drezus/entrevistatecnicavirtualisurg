using System.Linq;
using Unity.Netcode;
using Unity.VisualScripting;
using UnityEngine;

public abstract class BasePlayerMovement : BaseMovement
{
    protected GameManager gameManager;
    protected PlayerInputs inputs;
    private PlayerCollision coll;
    private SpikeCrosshairMovement spikeCrosshair;

    public uint PlayerID
    {
        get;
        private set;
    }

    protected BallMovement ball;

    //Players will not be able to spike balls that are below them.
    protected bool inSpikeRange => Vector3.Distance(thisTransform.position, ball.transform.position) <= gameParams.spikeRange;

    protected bool ballTargettingThisCourt => PlayerID == 0 ? ball.IsTargettingLeftCourt : !ball.IsTargettingLeftCourt;

    protected bool spikeAttempted = false;

    protected override void Awake()
    {
        base.Awake();

        ball = FindObjectOfType<BallMovement>();
        gameManager = FindObjectOfType<GameManager>();

        gameManager.OnCourtReset.AddListener(ResetToCourtCenter);
    }

    public override void OnDestroy()
    {
        OnLand.RemoveAllListeners();
        if(inputs) inputs.OnJumpPressed.RemoveAllListeners();
        gameManager.OnCourtReset.RemoveListener(ResetToCourtCenter);
        base.OnDestroy();
    }


    protected override void AssignGameParams()
    {
        gravity = gameParams.playerGravity;
        speed = gameParams.playerSpeed;
        verticalForce = gameParams.playerJumpForce;
        heightFromFloor = gameParams.playerHeightFromFloor;
    }

    public void SetupInstance(uint id, SpikeCrosshairMovement spkTgt)
    {
        PlayerID = id;
        spikeCrosshair = spkTgt;
        courtRect = PlayerID == 0 ? gameParams.leftCourtLimits : gameParams.rightCourtLimits;

        inputs = GetComponent<PlayerInputs>();
        coll = GetComponent<PlayerCollision>();

        OnLand.AddListener(Landing);
        if(inputs) inputs.OnJumpPressed.AddListener(JumpPressed);
    }

    protected void JumpPressed()
    {
        if (IsGrounded)
        {
            base.VerticalLaunch(verticalForce);
            if(ballTargettingThisCourt) spikeCrosshair.EnableSpikeAiming();
        }
        else
        {
            if (spikeAttempted || !inSpikeRange || spikeCrosshair.IsLocked) return;

            coll.ResetLobCount();
            spikeCrosshair.LockMarker();

            SpikeBall();

            spikeAttempted = true;
        }
    }

    private void SpikeBall()
    {
        if (gameManager.CurrentGameMode == GameModes.OnlineMultiplayer)
        {
            SpikeBallRpc();
        }
        else
        {
            ball.LobAtTarget(spikeCrosshair, 0f);
        }
    }

    [Rpc(SendTo.Everyone, RequireOwnership = false)]
    private void SpikeBallRpc()
    {
        ball.LobAtTarget(spikeCrosshair, 0f);
    }

    private void Landing()
    {
        spikeAttempted = false;

        if(spikeCrosshair == null || spikeCrosshair.IsLocked) return;
        spikeCrosshair.SetMarkerVisibility(false);
    }

    private void ResetToCourtCenter()
    {
        if (gameManager.CurrentGameMode == GameModes.OnlineMultiplayer)
        {
            ResetToCourtCenterRpc();
        }
        else
        {
            thisTransform.position = new Vector3(PlayerID == 0 ? gameParams.leftCourtLimits.x : gameParams.rightCourtLimits.x, gameParams.playerHeightFromFloor, 0f);
            coll.ResetLobCount();
        }
    }

    [Rpc(SendTo.Everyone, RequireOwnership = false)]
    private void ResetToCourtCenterRpc()
    {
        thisTransform.position = new Vector3(PlayerID == 0 ? gameParams.leftCourtLimits.x : gameParams.rightCourtLimits.x, gameParams.playerHeightFromFloor, 0f);
        coll.ResetLobCount();
    }
}