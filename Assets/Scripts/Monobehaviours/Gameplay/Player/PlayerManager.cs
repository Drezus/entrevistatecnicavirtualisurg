using System;
using System.Linq;
using Unity.Netcode;
using UnityEngine;

public class PlayerManager : MonoBehaviour
{
    private GameManager gameManager;

    public uint playerId
    {
        get;
        private set;
    }

    [SerializeField] private GameParameters gameParams;

    [SerializeField] private Renderer rend;
    public BasePlayerMovement pMove;
    public PlayerInputs pInputs;
    [SerializeField] private PlayerCollision pColl;

    InputSettings defaultKeys => gameParams.playerSettings.FirstOrDefault(p => p.id == 0).inputSet;

    public void Setup(uint id, GameManager gameMng)
    {
        gameManager = gameMng;

        playerId = id;

        transform.position = playerId == 0 ?
        new Vector3(gameParams.leftCourtLimits.x, gameParams.playerHeightFromFloor, gameParams.leftCourtLimits.y) :
        new Vector3(gameParams.rightCourtLimits.x, gameParams.playerHeightFromFloor, gameParams.rightCourtLimits.y);

        PlayerSettings thisPlayerSettings = gameParams.playerSettings.FirstOrDefault(p => p.id == playerId);

        if(pInputs) pInputs.Setup(gameManager.CurrentGameMode == GameModes.OnlineMultiplayer? defaultKeys : thisPlayerSettings.inputSet, gameManager.mobileInputCanvas, gameManager.joystick, gameManager.jumpBtn);

        pColl.SetupInstance(pMove, gameManager.lobMarker);

        Material thisMat = rend.material;
        thisMat.SetColor("_Color", thisPlayerSettings.color);
    }
}