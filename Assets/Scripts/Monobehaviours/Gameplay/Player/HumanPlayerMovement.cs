using System.Linq;
using Unity.Netcode;
using Unity.VisualScripting;
using UnityEngine;

public class HumanPlayerMovement : BasePlayerMovement
{
    private SpikeCrosshairMovement opponentSpikeMarker;

    protected override void Update()
    {
        Vector3 inputVec = Vector3.zero;

        inputVec = gameManager.canMovePlayers ? new Vector3(inputs.GetHorizontal(), 0f,
            inputs.GetVertical()) : Vector3.zero;

        //Players will only be able to control when ground.
        //When airborne, they'll carry over their momentum around.
        if (IsGrounded) velocity = inputVec;

        base.Update();
    }
}