using System.Linq;
using Unity.Netcode;
using UnityEngine;

public class SpikeCrosshairMovement : BaseMovement, ITargetMarker
{
    public uint PlayerID
    {
        get;
        private set;
    }

    public bool isVisible => markerImg.activeSelf;

    [SerializeField] private PlayerInputs inputs;

    private bool canMove = true;
    public bool IsLocked => !canMove && markerImg.activeSelf;

    [SerializeField]
    private GameObject markerImg;

    private bool isCPU = false;
    private Vector3 cpuRandomTargetPos = Vector3.zero;

    protected override void AssignGameParams()
    {
        gravity = 0f;
        verticalForce = 0f;
        speed = gameParams.markerSpeed;
        heightFromFloor = gameParams.ballHeightFromFloor;
    }

    public void Setup(uint id, bool cpu = false)
    {
        PlayerID = id;

        isCPU = cpu;

        PlayerSettings thisPlayerSettings = gameParams.playerSettings.FirstOrDefault(p => p.id == PlayerID);
        TintMarker(thisPlayerSettings.color);

        PlayerManager owner = FindObjectsOfType<PlayerManager>().FirstOrDefault(pm => pm.playerId == PlayerID);
        if (owner == null)
        {
            Debug.LogError($"Couldn't find an owner player for SpikeMarker with ID {id}");
            return;
        }

        courtRect = PlayerID == 0 ? gameParams.rightCourtLimits : gameParams.leftCourtLimits;

        inputs = owner.pInputs;
        owner.pMove.SetupInstance(PlayerID, this);
    }

    protected override void Update()
    {
        if (!canMove) return;
        if (!inputs && !isCPU) return;

        Vector3 inputVec = Vector3.zero;

        if (!isCPU)
        {
            inputVec = new Vector3(inputs.GetHorizontal(), 0f,
                inputs.GetVertical());
        }
        else
        {
            inputVec = (cpuRandomTargetPos - thisTransform.position).normalized;
        }

        velocity = inputVec;
        base.Update();
    }

    public void EnableSpikeAiming()
    {
        SetMarkerVisibility(true);
        thisTransform.position = new Vector3(courtRect.Value.x, heightFromFloor, courtRect.Value.y);
        velocity = Vector3.zero;
        canMove = true;

        if (!isCPU) return;

        cpuRandomTargetPos = new Vector3(
            Random.Range(courtRect.Value.x - (courtRect.Value.width / 2), courtRect.Value.x + (courtRect.Value.width / 2)),
            heightFromFloor,
            Random.Range(courtRect.Value.y - (courtRect.Value.height / 2), courtRect.Value.y + (courtRect.Value.height / 2)));
    }

    public void SetMarkerAtPos(Vector3 pos)
    {
        thisTransform.position = pos;
        velocity = Vector3.zero;
        canMove = false;
    }

    public void LockMarker()
    {
        velocity = Vector3.zero;
        canMove = false;
    }

    public void SetMarkerVisibility(bool visible)
    {
        if (IsSpawned)
        {
            SetMarkerVisibilityRpc(visible);
        }
        else
        {
            markerImg.SetActive(visible);
        }
    }

    [Rpc(SendTo.Everyone, RequireOwnership = false)]
    private void SetMarkerVisibilityRpc(bool visible)
    {
        markerImg.SetActive(visible);
    }

    private void TintMarker(Color col)
    {
        markerImg.GetComponent<Renderer>().material.SetColor("_Color", col);
    }
}