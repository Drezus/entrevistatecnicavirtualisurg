using System;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;

public class PlayerInputs : MonoBehaviour
{
    private GameManager gameManager;
    private InputSettings inputKeys;
    private Canvas mobileInputCanvas;
    private MobileJoystick mobileJoystick;
    private Button jumpBtn;

    public sealed class PlayerInputEvent : UnityEvent { };
    public readonly PlayerInputEvent OnJumpPressed = new PlayerInputEvent();

    private void Awake()
    {
        gameManager = FindObjectOfType<GameManager>();
    }

    public void Setup(InputSettings keySettings, Canvas canv, MobileJoystick joystk, Button btn)
    {
        inputKeys = keySettings;
        mobileInputCanvas = canv;
        mobileJoystick = joystk;
        jumpBtn = btn;

        jumpBtn.onClick.AddListener(JumpPressed);
    }

    private void OnDestroy()
    {
       if(jumpBtn) jumpBtn.onClick.RemoveAllListeners();
    }

    public void Update()
    {
        if (!gameManager.canMovePlayers) return;
        if(Input.GetButtonDown(inputKeys.jump)) OnJumpPressed?.Invoke();
    }

    private void JumpPressed()
    {
        if (!gameManager.canMovePlayers) return;
        OnJumpPressed?.Invoke();
    }

    public float GetHorizontal()
    {
        if (!gameManager.canMovePlayers) return 0;
        return mobileInputCanvas.gameObject.activeSelf ? mobileJoystick.Horizontal : Input.GetAxis(inputKeys.horizontal);
    }

    public float GetVertical()
    {
        if (!gameManager.canMovePlayers) return 0;
        return mobileInputCanvas.gameObject.activeSelf ? mobileJoystick.Vertical : Input.GetAxis(inputKeys.vertical);
    }
}