using System.Linq;
using Unity.Netcode;
using Unity.VisualScripting;
using UnityEngine;

public class CPUPlayerMovement : BasePlayerMovement
{
    private SpikeCrosshairMovement opponentSpikeMarker;

    protected override void Update()
    {
        Vector3 inputVec = Vector3.zero;

        if(opponentSpikeMarker == null) opponentSpikeMarker = FindObjectsOfType<SpikeCrosshairMovement>().FirstOrDefault(sm => sm.PlayerID != PlayerID);

        Vector3? targetPos = null;

        if (opponentSpikeMarker.isVisible)
        {
            targetPos = opponentSpikeMarker.transform.position;
        }
        else if (gameManager.lobMarker.isVisible && ballTargettingThisCourt)
        {
            targetPos = gameManager.lobMarker.transform.position;
        }

        inputVec = gameManager.canMovePlayers && targetPos.HasValue ?
            (targetPos.Value - thisTransform.position).normalized :
            Vector3.zero;

        if (IsGrounded)
        {
            velocity = inputVec;

            //CPU only goes for spikes on lobbed balls
            if (targetPos.HasValue && gameManager.lobMarker.isVisible && ballTargettingThisCourt)
            {
                if (Vector3.Distance(targetPos.Value, thisTransform.position) <= 0.12f && ball.isFalling)
                {
                    JumpPressed();
                }
            }
        }
        else
        {
            if(inSpikeRange && !spikeAttempted) JumpPressed();
        }

        base.Update();
    }
}