using UnityEngine;

public interface ITargetMarker
{
    public void SetMarkerAtPos(Vector3 pos);
    public void SetMarkerVisibility(bool visible);

}