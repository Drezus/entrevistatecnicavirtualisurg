using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.Events;

public class BallMovement : BaseMovement
{
    private ITargetMarker currentMarker;
    public bool hasTarget => currentMarker != null;

    public bool IsTargettingLeftCourt => hasTarget && ((MonoBehaviour)currentMarker).transform.position.x <= 0;

    public sealed class VolleyballEvent : UnityEvent { };
    public readonly VolleyballEvent OnNetHit = new VolleyballEvent();

    protected override void AssignGameParams()
    {
        gravity = gameParams.ballGravity;

        speed = 1f; //Velocities calculated during CalculateArcTrajectory() are always based on a speed value of 1.
        verticalForce = 0f; //Provided by players hitting the ball, varies by action.
        heightFromFloor = gameParams.ballHeightFromFloor;
    }

    private void FixedUpdate()
    {
        if (freezePhysics) return;

        if(thisTransform.position.x is <= 0.1f and >= -0.1f
           && thisTransform.position.y <= 0.6f)
            OnNetHit?.Invoke();
    }

    public void LobAtTarget(ITargetMarker target, float arcHeight)
    {
        if (freezePhysics) freezePhysics = false;

        if (currentMarker != null) DisposeFromMarker();

        currentMarker = target;
        Vector3 targetPos = ((MonoBehaviour)currentMarker).transform.position;

        currentMarker.SetMarkerAtPos(targetPos);

        OnLand.AddListener(DisposeFromMarker);
        Vector3 arc = CalculateArcTrajectory(targetPos, arcHeight);

        velocity = new Vector3(arc.x, 0f, arc.z);
        base.VerticalLaunch(arc.y);
    }

    private Vector3 CalculateArcTrajectory(Vector3 targetPos, float height)
    {
        if(!thisTransform) Awake();

        Vector3 currentPos = thisTransform.position;

        Vector3 xzDisplacement = new Vector3 (targetPos.x - currentPos.x, 0, targetPos.z - currentPos.z);
        float yDisplacement = targetPos.y - currentPos.y;

        float duration = Mathf.Sqrt(-2 * height / gravity) + Mathf.Sqrt(2 * (yDisplacement - height) / gravity);

        Vector3 yVel = Vector3.up * Mathf.Sqrt(-2 * gravity * height);
        Vector3 xzVel = xzDisplacement / duration;

        return xzVel + yVel;
    }

    private void DisposeFromMarker()
    {
        currentMarker.SetMarkerVisibility(false);
        currentMarker = null;
        OnLand.RemoveListener(DisposeFromMarker);
    }
}