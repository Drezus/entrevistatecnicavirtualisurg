using Unity.Netcode;
using UnityEngine;

public class LobTarget : NetworkBehaviour, ITargetMarker
{
    [SerializeField]
    private Transform thisTransform;

    [SerializeField]
    private GameObject markerImg;

    public bool isVisible => markerImg.activeSelf;

    public void SetMarkerAtPos(Vector3 pos)
    {
        SetMarkerVisibility(true);
        thisTransform.position = pos;
    }

    public void SetMarkerVisibility(bool visible)
    {
        if (IsSpawned)
        {
            SetMarkerVisibilityRpc(visible);
        }
        else
        {
            markerImg.SetActive(visible);
        }
    }

    [Rpc(SendTo.Everyone, RequireOwnership = false)]
    protected void SetMarkerVisibilityRpc(bool visible)
    {
        markerImg.SetActive(visible);
    }
}