using System;
using Unity.Netcode;
using UnityEngine;
using UnityEngine.Events;

[RequireComponent(typeof(Transform))]
public abstract class BaseMovement : NetworkBehaviour
{
    protected GameParameters gameParams;

    protected Transform thisTransform;
    protected Vector3 velocity;

    public bool IsGrounded { get; private set; }

    protected float speed;
    protected float verticalForce;
    protected float gravity = Physics.gravity.y;
    protected float heightFromFloor;
    protected Rect? courtRect; //Optional. If provided by inherited classes, it'll be used to limit their movement to a certain area.

    public sealed class MovementEvent : UnityEvent { };
    public readonly MovementEvent OnLand = new MovementEvent();

    [HideInInspector] public bool freezePhysics;

    public bool isFalling => velocity.y < 0;

    protected virtual void Awake()
    {
        //Caching transform component reference for improved performance.
        thisTransform = transform;
        OnLand.AddListener(Land);

        gameParams = FindObjectOfType<GameManager>().gameParams;
        AssignGameParams();
    }

    protected abstract void AssignGameParams();

    public override void OnDestroy()
    {
        OnLand.RemoveListener(Land);
        base.OnDestroy();
    }

    protected virtual void Update()
    {
        if (freezePhysics) return;

        Vector3 horizontalVel = new Vector3(velocity.x * (Time.deltaTime * speed),
                                             velocity.y * Time.deltaTime,
                                             velocity.z * (Time.deltaTime * speed));
        transform.Translate(horizontalVel, Space.World);

        //Position clamping. Vertically for all instances of BaseMovement, and laterally for any instance providing a CourtRect.
        Vector3 newPos = thisTransform.position;
        if (courtRect.HasValue)
        {
            newPos.x = Mathf.Clamp(newPos.x, courtRect.Value.x - (courtRect.Value.width/2), courtRect.Value.x + (courtRect.Value.width/2));
            newPos.z = Mathf.Clamp(newPos.z, courtRect.Value.y - (courtRect.Value.height/2), courtRect.Value.y + (courtRect.Value.height/2));
        }
        newPos.y = Mathf.Clamp(newPos.y, heightFromFloor, 10f);
        thisTransform.position = newPos;

        if (IsGrounded) return;

        if (transform.position.y <= heightFromFloor)
        {
            IsGrounded = true;
            OnLand?.Invoke();
        }
        else
        {
            velocity.y += gravity * Time.deltaTime;
        }
    }

    protected void VerticalLaunch(float force)
    {
        IsGrounded = false;
        velocity.y = force;
    }

    protected virtual void Land()
    {
        velocity = Vector3.zero;
    }
}